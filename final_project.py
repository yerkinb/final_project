import pandas
import matplotlib.pyplot as plt
import plotly.express as px
import plotly.graph_objects as go
import numpy
import datetime as dt
from scipy.optimize import curve_fit
import streamlit as st

st.subheader("Reservoir Monitoring Dashboard")
t1, t2, t3, t4=st.tabs(["🌐Contour Map", "📉Historical Plot", "🔴Heterogeneity Index", "↗️Decline Curve Analysis"])

file = "production_history.txt"
prod_table = pandas.read_csv(file, sep="\t")
prod_table["Date"] = pandas.to_datetime(prod_table['Date'])
wells = prod_table["UID"].unique()

# History Plot starts here

#single selection
#selected_well = st.selectbox("Please Select a well", wells)
#multiple selection
selected_well = t2.multiselect("Please Select a well", wells)

#st.write(selected_well)

#prod_filt = prod_table[ prod_table["UID"] == selected_well]
prod_filt = prod_table[ prod_table["UID"].isin(selected_well)]
#st.dataframe( prod_filt)

variables = prod_filt.columns

variable_selected = t2.selectbox("Please Select a Variable", variables)

df_group = prod_filt.groupby("Date").sum()
df_group["Date"] = df_group.index


fig_prod_group = px.line(df_group, x="Date", y=variable_selected)

fig_prod = px.line(prod_filt, x="Date", y=variable_selected, color="UID")
t2.plotly_chart( fig_prod, use_container_width=True)

t2.plotly_chart( fig_prod_group, use_container_width=True)

# Contour Map starts here

df_xy = pandas.read_csv("pr_xy.txt", sep="\t")

#t1.write(df_xy.columns)

fig, ax = plt.subplots()
x = df_xy["BOTTOMX"]
y = df_xy["BOTTOMY"]
z = df_xy["TDEPTH"] 

ax.tricontourf(x, y, z)
ax.scatter(x, y, color="black", marker="^")
ax.grid(alpha=0.2, color="black")

number_rows = len (df_xy)

for i in range (number_rows):
    name = df_xy["UID"][i]
    x_coord = df_xy["BOTTOMX"][i]
    y_coord = df_xy["BOTTOMY"][i]
    ax.text(x_coord, y_coord+1000, name, color="yellow", fontsize=5)

t1.subheader("Contour Map")
t1.pyplot(fig, use_container_width=True, )

# Heterogeneity Index Analysis
unique_dates = prod_table["Date"].unique()
date_select = t3.selectbox("PLease select a Date to Analyze", unique_dates)

df_date_filt = prod_table[prod_table["Date"] == date_select]

# HI = (value/group_avg) - 1
avg_oil = df_date_filt["OIL"].mean()
avg_gas = df_date_filt["GAS"].mean()
avg_water = df_date_filt["WATER"].mean()

df_date_filt["HI-OIL"] = (df_date_filt["OIL"]/avg_oil) - 1
df_date_filt["HI-GAS"] = (df_date_filt["GAS"]/avg_gas) - 1
df_date_filt["HI-WATER"] = (df_date_filt["WATER"]/avg_water) - 1

c1, c2, c3 = t3.columns(3)

c1.metric("Average Oil Prod", format(avg_oil, ".1f"), "STB/D")
c2.metric("Average Gas Prod", format(avg_gas, ".1f"), "MSCF")
c3.metric("Average Water Prod", int(avg_water), "STB/D")

fig_hi = px.scatter(df_date_filt, x="HI-OIL", y="HI-WATER", color="UID", template="none", width=800, height=700, size="OIL")
t3.plotly_chart(fig_hi, use_container_width=True)

# Decline Curve Analysis

st.subheader("DCA Dashboard")
t5, t6, t7 = st.tabs(["Manual Fitting", "Automated Fitting", "Forecast Values"])

def dca_for_fit(t, qi, Di, b):
    q = qi / (1+(b*Di*t))**(1/b)
    return q

well_selection = st.sidebar.selectbox("Please Select a well", wells)

st.sidebar.write("__________")
qi_input = st.sidebar.slider("Qi Input:",0,25000,value=10000)
b_input = st.sidebar.slider("b Input:",0.0,3.0,value=0.5)
di_input = st.sidebar.slider("Di Input:",0.0,1.0,value=0.0005)
rate_filter = st.sidebar.number_input("Rule out Rates", value=100)
prod_table = prod_table[prod_table["OIL"] >= rate_filter]


df_well = prod_table[ prod_table.UID == well_selection]
#st.write(df_well)
df_well.reset_index(inplace=True)
t_months = numpy.array(df_well.index)

q_calc = dca_for_fit(t_months, qi_input, di_input, b_input)
#st.write(q_calc)

date_selected = st.sidebar.select_slider("Please Select Dates", df_well.Date)
df_well = df_well[df_well["Date"] >= date_selected ]

fig_dca = px.scatter(df_well, x = "Date", y = "OIL", template = "none", title = well_selection)

line = go.Line(x=df_well.Date, y=q_calc, name="DCA fit (manual)")
fig_dca.add_trace(line)

# Forecasting

months_forecast = st.sidebar.number_input("Number of months to predict", value=60)
start_prod = df_well["Date"].min()
future_dates = []
total_forecasted_m = []
elapsed_m = len(df_well)

for month in range(elapsed_m, elapsed_m + months_forecast ):
    new_date = start_prod + pandas.DateOffset(months=month)
    total_forecasted_m.append(month)
    future_dates.append(new_date)

total_forecasted_m = numpy.array(total_forecasted_m)
q_forecast = dca_for_fit(total_forecasted_m, qi_input, di_input, b_input)

line_forecast = go.Line(x=future_dates, y=q_forecast, name="DCA Forecast", line_color="green")
fig_dca.add_trace(line_forecast)

table_forecast = pandas.DataFrame({"Date":future_dates, "Oil Rate Forecast":q_forecast})
table_forecast["Cumulative Produced Oil"] = table_forecast["Oil Rate Forecast"].cumsum()

t7.dataframe(table_forecast)

t5.plotly_chart(fig_dca, use_container_width=True)

# Automated Fitting

df_well_auto = prod_table[ prod_table.UID == well_selection]
df_well_auto.reset_index(inplace=True)

popt, pcov = curve_fit(dca_for_fit, df_well_auto.index, df_well_auto.OIL)

qi = popt[0]
di = popt[1]
b = popt[2]
t_months_auto = numpy.array(df_well_auto.index)
q_calc_auto = dca_for_fit(t_months_auto, qi, di, b)

fig_auto = px.scatter(df_well_auto, x="Date", y="OIL", template = "none", title = well_selection)
line_auto = go.Line(x=df_well_auto.Date, y=q_calc_auto, name="DCA fit (auto)")
fig_auto.add_trace(line_auto)

t6.plotly_chart(fig_auto, use_container_width=True)
t6.write(popt)